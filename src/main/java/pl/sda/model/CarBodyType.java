package pl.sda.model;

public enum CarBodyType {
    SEDAN,
    COMBI,
    CABRIO
}
