package pl.sda.model;

import lombok.*;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    String brand;

    String model;

    CarBodyType carBodyType;

    LocalDate productionDate;

    @Formula(value = "YEAR(NOW())-YEAR(productionDate)")
    Long age;

    String color;

    Long km;

    @UpdateTimestamp
    LocalDateTime modifiedDate;
}
