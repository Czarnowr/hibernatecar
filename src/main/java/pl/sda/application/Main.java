package pl.sda.application;

import pl.sda.logic.CarDao;
import pl.sda.logic.HibernateUtil;
import pl.sda.model.Car;
import pl.sda.model.CarBodyType;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        CarDao carDao = new CarDao();

        boolean working = true;
        String input;

        do {
            System.out.println("Enter instruction:\nadd\nget/getByAge/getByKm\nlistOrdered/listLimitedAndOrdered\nupdate\ndelete\nexit");
            input = scanner.nextLine();

            switch (input) {
                case "add":
                    Car car = new Car();

                    enterCarInformation(car);

                    carDao.saveOrUpdateCar(car);
                    break;

                case "get":
                    System.out.println("Car Id: ");
                    String id = scanner.nextLine();
                    Optional<Car> optionalCar = carDao.getCarById(Long.valueOf(id));
                    if (optionalCar.isPresent()) {
                        Car carById = optionalCar.get();
                        System.out.println(carById);
                    } else {
                        System.out.println("Car not found.");
                    }
                    break;

                case "getByAge":
                    System.out.println("Choose: youngest/oldest");
                    String age = scanner.nextLine();
                    List<Car> youngestOrOldest = carDao.getYoungestOrOldest(age);
                    youngestOrOldest.forEach(System.out::println);
                    break;

                case "getByKm":
                    System.out.println("Choose km: least/most");
                    String mileage = scanner.nextLine();
                    List<Car> leastOrMostDriven = carDao.getMostOrLeastDriven(mileage);
                    leastOrMostDriven.forEach(System.out::println);
                    break;

                case "listOrdered":
                    System.out.println("Order by: id/brand/model/carBodyType/productionDate/color/km/modifiedDate");
                    String orderBy = scanner.nextLine();
                    List<Car> carList = carDao.getAllCars(orderBy);
                    carList.forEach(System.out::println);
                    break;

                case "listLimitedAndOrdered":
                    System.out.println("Limit by: id/brand/model/carBodyType/productionDate/color/km/modifiedDate");
                    String limiterType = scanner.nextLine();
                    System.out.println("Enter searched value: ");
                    String limiterValue = scanner.nextLine();
                    System.out.println("Order by: id/brand/model/carBodyType/productionDate/color/km/modifiedDate");
                    String limitAndOrderBy = scanner.nextLine();
                    List<Car> limitedCarList = carDao.getAllCars(limiterType, limiterValue, limitAndOrderBy);
                    limitedCarList.forEach(System.out::println);
                    break;
                case "update":
                    System.out.println("Car Id to update: ");
                    String updateId = scanner.nextLine();
                    Optional<Car> optionalCarToUpdate = carDao.getCarById(Long.valueOf(updateId));
                    if (optionalCarToUpdate.isPresent()) {
                        Car carToUpdate = optionalCarToUpdate.get();
                        enterCarInformation(carToUpdate);
                        carDao.saveOrUpdateCar(carToUpdate);
                    } else {
                        System.out.println("Car not found.");
                    }
                    break;

                case "delete":
                    System.out.println("Car Id to delete: ");
                    String deleteId = scanner.nextLine();
                    carDao.removeCar(Long.valueOf(deleteId));
                    break;

                case "exit":
                    working = false;
                    System.out.println("Thank you for using the Car Workshop App!");
                    break;

                default:
                    System.out.println("Input not recognised, try again");
                    break;
            }
        } while (working);

        HibernateUtil.getSessionFactory().close();
    }

    private static void enterCarInformation(Car car) {
        System.out.println("Brand:");
        String brand = scanner.nextLine();
        car.setBrand(brand);

        System.out.println("Model:");
        String model = scanner.nextLine();
        car.setModel(model);

        System.out.println("Body Type: combi/sedan/cabrio");
        String bodyType = scanner.nextLine();
        car.setCarBodyType(CarBodyType.valueOf(bodyType.toUpperCase()));

        System.out.println("Production Date (year):");
        String year = scanner.nextLine();
        System.out.println("Production Date (month):");
        String month = scanner.nextLine();
        System.out.println("Production Date (day):");
        String day = scanner.nextLine();
        car.setProductionDate(LocalDate.of(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day)));

        System.out.println("Color:");
        String color = scanner.nextLine();
        car.setColor(color);

        System.out.println("Km:");
        String km = scanner.nextLine();
        car.setKm(Long.valueOf(km));
    }
}
