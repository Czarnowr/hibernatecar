package pl.sda.logic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.model.Car;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class CarDao {

    public void saveOrUpdateCar(Car car) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;

        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.saveOrUpdate(car);

            transaction.commit();
        } catch (Exception sqle) {
            Logger.getLogger(getClass().getName()).info("Input error" + sqle.getMessage());
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public Optional<Car> getCarById(Long carId) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        try (Session session = sessionFactory.openSession()) {

            Car car = session.get(Car.class, carId);
            return Optional.of(car);
        } catch (Exception sqle) {
            Logger.getLogger(getClass().getName()).info("Input error" + sqle.getMessage());
        }
        return Optional.empty();
    }

    public List<Car> getAllCars(String orderBy) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        List<Car> listOfCars = new ArrayList<>();

        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            CriteriaQuery<Car> query = builder.createQuery(Car.class);

            Root<Car> tableRoot = query.from(Car.class);

            if (EnumTest.contains(orderBy)) {
                query.select(tableRoot).orderBy(builder.asc(tableRoot.get(orderBy)));
            } else {
                System.out.println("Incorrect input for orderBy");
            }

            return session.createQuery(query).getResultList();
        } catch (Exception sqle) {
            System.err.println("Error: " + sqle.getMessage());
        }

        return listOfCars;
    }

    public List<Car> getAllCars(String table, String containing, String orderBy) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        List<Car> listOfCars = new ArrayList<>();

        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            CriteriaQuery<Car> query = builder.createQuery(Car.class);

            Root<Car> tableRoot = query.from(Car.class);

            if (EnumTest.contains(orderBy) && EnumTest.contains(table)) {
                query.select(tableRoot).where(builder.equal(tableRoot.get(table), containing)).orderBy(builder.asc(tableRoot.get(orderBy)));
            } else {
                System.out.println("Incorrect input for orderBy");
            }

            return session.createQuery(query).getResultList();
        } catch (Exception sqle) {
            System.err.println("Error: " + sqle.getMessage());
        }

        return listOfCars;
    }

    private Long getLowestOrHighestValue(String valueType, String lowestOrHighest) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Long result = -1L;

        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            CriteriaQuery<Long> query = builder.createQuery(Long.class);

            Root<Car> tableRoot = query.from(Car.class);

            if (lowestOrHighest.equalsIgnoreCase("highest")) {
                query.select(builder.max(tableRoot.get(valueType)));
            } else {
                query.select(builder.min(tableRoot.get(valueType)));
            }

            return session.createQuery(query).getSingleResult();
        } catch (Exception sqle) {
            System.err.println("Error: " + sqle.getMessage());
        }

        return result;
    }

    public List<Car> getYoungestOrOldest(String choice) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        List<Car> listOfCars = new ArrayList<>();

        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            CriteriaQuery<Car> query = builder.createQuery(Car.class);

            Root<Car> tableRoot = query.from(Car.class);

            if (choice.equalsIgnoreCase("oldest")) {
                Long age = getLowestOrHighestValue("age", "highest");
                query.select(tableRoot).where(builder.equal(tableRoot.get("age"), age));
            } else if (choice.equalsIgnoreCase("youngest")) {
                Long age = getLowestOrHighestValue("age", "lowest");
                query.select(tableRoot).where(builder.equal(tableRoot.get("age"), age));
            } else {
                System.out.println("incorrect input for youngest/oldest");
            }

            return session.createQuery(query).getResultList();
        } catch (Exception sqle) {
            System.err.println("Error: " + sqle.getMessage());
        }

        return listOfCars;
    }

    public List<Car> getMostOrLeastDriven(String choice) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        List<Car> listOfCars = new ArrayList<>();

        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            CriteriaQuery<Car> query = builder.createQuery(Car.class);

            Root<Car> tableRoot = query.from(Car.class);

            if (choice.equalsIgnoreCase("most")) {
                Long mileage = getLowestOrHighestValue("km", "highest");
                query.select(tableRoot).where(builder.equal(tableRoot.get("km"), mileage));
            } else if (choice.equalsIgnoreCase("least")) {
                Long mileage = getLowestOrHighestValue("km", "lowest");
                query.select(tableRoot).where(builder.equal(tableRoot.get("km"), mileage));
            } else {
                System.out.println("incorrect input for least/most");
            }

            return session.createQuery(query).getResultList();
        } catch (Exception sqle) {
            System.err.println("Error: " + sqle.getMessage());
        }

        return listOfCars;
    }

    public void removeCar(Long carId) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;

        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            Optional<Car> optionalCar = getCarById(carId);
            optionalCar.ifPresent(session::delete);

            transaction.commit();
        } catch (Exception sqle) {
            Logger.getLogger(getClass().getName()).info("Input error" + sqle.getMessage());
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
}
