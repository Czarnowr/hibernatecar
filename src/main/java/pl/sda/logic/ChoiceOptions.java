package pl.sda.logic;

public enum ChoiceOptions {
    id,
    brand,
    model,
    carBodyType,
    productionDate,
    color,
    km,
    modifiedDate

}
