package pl.sda.logic;

public class EnumTest {
    public static boolean contains(String test) {

        for (ChoiceOptions c : ChoiceOptions.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }

        return false;
    }
}
